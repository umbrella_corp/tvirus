# ¡Bienvenido al T-Virus Project!

Este proyecto hemos intentado inspirarlo en Resident Evil  por ello nos han encomendado la misión de crear un software basado en Java que permite añadir más seguridad con el cifrado y encriptación de ficheros con la formula del virus, conexiones, claves, etc... 


# Estructura del proyecto
Este software encomendado por ***@teacherdev5*** sigue unas pautas genéricas que encontramos en un [documento PDF](https://www.vidalibarraquer.net/moodle/pluginfile.php/63307/mod_assign/introattachment/0/Enunciat%20projecte%20m9uf1.pdf?forcedownload=1) en un lugar con alta contaminación del virus llamado **Moodle**. 

## Paquetes del proyecto

**Como paquetes en este proyecto tenemos:**

### TVirus
En este paquete podremos encontrar un menú el cual el usuario podrá eligir entre hacer **operaciones Hash o cifrar/descifrar documentación y ficheros** importantes del ***Umbrella Corp***. Dentro de cada uno de ellos obtendremos más menús para que puedas realizar la tarea estimada.
#### Generar Hash
##### Funcionamiento
![enter image description here](assets/GenerarHash.gif "Generar Hash")
#### Comprobación Hash: Desde fichero
##### Funcionamiento
![enter image description here](assets/ComprobacionHashFichero.gif "ComprobacionHashFichero")
#### Comprobación Hash: Desde teclado
##### Funcionamiento
![enter image description here](assets/CoprobarHashTeclado.gif "CoprobarHashTeclado")
#### Cifrar ficheros
##### Funcionamiento
![enter image description here](assets/CifrarFichero.gif "CifrarFichero")
#### Descifrar ficheros
##### Funcionamiento
![enter image description here](assets/DescrifrarFichero.gif "DescifrarFichero")

### Requisitos deseados
#### Criptografía asimétrica
En este punto del proyecto ***@teacherdev5*** nos encomendó la tarea de cifrar y descifrar archivos secretos mediante un Cliente y Servidor, siendo el Servidor de ***Umbrella Corp*** el que nos suministre de la clave pública y privada.

##### Funcionamiento
![enter image description here](assets/CriptografiaAsimetrica.gif "Criptografia Asimetrica")
#### Comunicación segura
El siguiente paso que nos encomendó fue la de establecer una conexión segura entre Cliente y Servidor para que ***Umbrella Corp*** no sufra un ataque *Man In The Middle* y sufra robo de información clasificada.

##### Funcionamiento
![enter image description here](assets/ConexionSegura.gif "ConexionSegura")

#### Firma digital
Por último, para mayor seguridad utilizamos lo último en seguridad en ficheros y es la firma digital. Con esto conseguiremos aumentar la seguridad y la integridad del mismo.
##### Funcionamiento
![enter image description here](assets/FirmaDigital.gif "FirmaDigital")

## Organización y repartición de tareas.

|Asignada a| Tareas |
|--|--|
| **David (@XRuppy)** | Cifrar un archivo (Criptografía simétrica AES), Descifrar un archivo (Con criptografía simétrica AES) y Intercambio de la clave secreta AES entre un Servidor - Cliente.
| **Carlos (@39913699m)** | Generar y almacenar un hash a un archivo y Conexión Cliente - Servidor segura (SSL) |
| **Isaac (@39920459a)**| Comprobar el hash 1: Introducción del Hash (Formato hexadecimal) por teclado, Comprobar el hash 2: Utilizando un archivo y Firma digital |

# Herramientas utilizadas

 - NetBeans 8.2
 - GitLab
 - Stackedit

## Componentes del grupo

|Nombre/NickID| Correo Electrónico | 
|--|--|
| David García Donaire / @XRuppy | 48021058w@vidalibarraquer.net |
| Carlos Solís Caño / @39913699m| 39913699m@vidalibarraquer.net |
| Isaac Martínez Moreno / @39920459a| 39920459a@vidalibarraquer.net |

