/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tvirus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Carlos, Isaac y David
 */
public class TVirus {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Scanner teclado = new Scanner(System.in);
        menuPrincipal(teclado);
    }

    /**
     * Este método sirve para generar un menú el cual nos dejaría hacer las
     * opciones que nos pide el enunciado requerido (Métodos). Este menú
     * (SwitchCase) esta hecho dentro de un bucle While para que terminé de dar
     * opciones cuando el usuario lo desee y si dicha opción no existe se lo
     * notificará.
     *
     * @param teclado Sirve para que el usuario pueda introducir sus opciones.
     * @throws IOException
     */
    public static void menuPrincipal(Scanner teclado) throws IOException {
        boolean salir = false;
        int opcion = 0;
        while (!salir) {
            System.out.println("Introduce una opcion:");
            System.out.println("---------------------");
            System.out.println("1) Operaciones con función Hash");
            System.out.println("2) Cifraje simétrico");
            System.out.println("3) Salir");
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1:
                    SubMenuOpcion1(teclado);
                    break;
                case 2:
                    SubMenuOpcion2(teclado);
                    break;
                case 3:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no disponible, por favor, introduce un número del 1 al 3");
            }
        }
    }

    /**
     * Este método sirve para generar un submenú dentro del menú anterior el
     * cual nos dejaría hacer las opciones que tendría la opción 1 del menú
     * anterior(Métodos). Este menú (SwitchCase) esta hecho dentro de un bucle
     * While para que terminé de dar opciones cuando el usuario lo desee y si
     * dicha opción no existe se lo notificará.
     *
     * @param teclado Sirve para que el usuario pueda introducir sus opciones.
     */
    public static void SubMenuOpcion1(Scanner teclado) {
        boolean salir = false;
        int opcion = 0;
        while (!salir) {
            System.out.println("Introduce una opcion:");
            System.out.println("---------------------");
            System.out.println("1) Generación y almacenamiento de Hash");
            System.out.println("2) Comprobación de Hash: Introducción por teclado");
            System.out.println("3) Comprobación de Hash II: Utilizando un fichero");
            System.out.println("4) Salir");
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1:
                    generacioHash();
                    break;
                case 2:
                    lecturaHash();
                    break;
                case 3:
                    lecturaHashArchivo();
                    break;
                case 4:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no disponible, por favor, intruduce un numero del 1 al 4");
            }
        }
    }

    /**
     * Este método sirve para generar un submenú dentro del menú anterior el
     * cual nos dejaría hacer las opciones que tendría la opción 2 del menú
     * anterior(Métodos). Este menú (SwitchCase) esta hecho dentro de un bucle
     * While para que terminé de dar opciones cuando el usuario lo desee y si
     * dicha opción no existe se lo notificará.
     *
     * @param teclado Sirve para que el usuario pueda introducir sus opciones.
     */
    public static void SubMenuOpcion2(Scanner teclado) {
        boolean salir = false;
        int opcion = 0;
        while (!salir) {
            System.out.println("Introduce una opcion:");
            System.out.println("---------------------");
            System.out.println("1) Cifrar un fichero");
            System.out.println("2) Descifrar un fichero");
            System.out.println("3) Salir");
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1:
                    File crearDirectorioResources = new File("resources/");
                    crearDirectorioResources.mkdirs();
                    CifrarFichero(teclado);
                    break;
                case 2:
                    DescifrarFichero(teclado);
                    break;
                case 3:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no disponible, por favor, introduce un numero del 1 al 3");
            }
        }
    }

    /**
     * Este método siguiendo las pautas del enunciado haría la opción de Cifrar
     * un fichero. Este método le pediría al usuario que archivo quiere cifrar
     * (IMPORTANTE, ESTE FICHERO DEBE ESTAR EN LA CARPERTA RESOURCES) y
     * porteriormente la contraseña que desea para cifrar. Un vez pedido al
     * usuario lo anterior, el método creará la carpeta crypt para que
     * posteriormente sea enviado ahí el fichero cifrado. Con Cipher eligiremos
     * el modo de cifrado y la contraseña elegida por el usuario.
     *
     * @param teclado Sirve para que el usuario pueda decirle al método que archivo y contraseña va a utilizar.
     */
    public static void CifrarFichero(Scanner teclado) {
        SecretKey key;
        Cipher cipher;
        int tamanoKey = 16;
        System.out.println("Introduce el nombre del fichero que quieres CIFRAR: ");
        String nombreFichero = teclado.next();

        System.out.println("Introduce la contraseña que has utilizado para CIFRAR. ¡IMPORTANTE! Acuérdate de ella si no perderás la información del fichero... ");
        String password = teclado.next();
        try {
            File crearDirectorioCrypt = new File("crypt/");
            crearDirectorioCrypt.mkdirs();
            FileOutputStream fos = new FileOutputStream("crypt/" + nombreFichero + ".aes");
            byte[] valuebytes = password.getBytes();
            key = new SecretKeySpec(Arrays.copyOf(valuebytes, tamanoKey), "AES");

            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            FileInputStream filein = new FileInputStream("resources/" + nombreFichero);

            CipherOutputStream out = new CipherOutputStream(fos, cipher);
            int midaBloc = cipher.getBlockSize();
            byte[] bytes = new byte[midaBloc];

            int i = filein.read(bytes);
            while (i != -1) {
                out.write(bytes, 0, i);
                i = filein.read(bytes);
            }
            out.flush();
            out.close();
            filein.close();
            System.out.println("Fichero cifrado con clave secreta.");
        } catch (IOException
                | InvalidKeyException
                | NoSuchAlgorithmException
                | NoSuchPaddingException e) {
            System.out.println(e.toString());
        }

    }

    /**
     * Este método siguiendo las pautas del enunciado haría la opción de Descifrar
     * un fichero. Este método le pediría al usuario que archivo quiere descifrar
     * (IMPORTANTE, ESTE FICHERO DEBE ESTAR EN LA CARPERTA CRYPT) y
     * porteriormente la contraseña que establecio para cifrar. Un vez pedido al
     * usuario lo anterior, el método creará la carpeta crypt por si no estubierá. Con Cipher eligiremos
     * el modo de descifrado y la contraseña elegida por el usuario. Por último creará el fichero descifrado en extensión .clar como pide el enunciado.
     *
     * @param teclado Sirve para que el usuario pueda decirle al método que archivo y contraseña va a utilizar.
     */
    public static void DescifrarFichero(Scanner teclado) {
        SecretKey key;
        Cipher cipher;
        int tamanoKey = 16;
        System.out.println("Introduce el nombre del fichero que quieres DESCIFRAR: ");
        String nombreFichero = teclado.next();

        System.out.println("Introduce la contraseña que quieres utilizar para DESCIFRAR. ¡IMPORTANTE! Acuérdate de ella si no perderás la información del fichero... ");
        String password = teclado.next();

        try {
            byte[] valuebytes = password.getBytes();
            key = new SecretKeySpec(Arrays.copyOf(valuebytes, tamanoKey), "AES");

            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);

            File crearDirectorioCrypt = new File("crypt/");
            crearDirectorioCrypt.mkdirs();

            CipherInputStream in = new CipherInputStream(
                    new FileInputStream("crypt/" + nombreFichero), cipher);
            int tambloque = cipher.getBlockSize();
            byte[] bytes = new byte[tambloque];
            tambloque = cipher.getBlockSize();
            bytes = new byte[tambloque];

            FileOutputStream fileout = new FileOutputStream("crypt/" + nombreFichero + ".clar");

            int i = in.read(bytes);
            while (i != -1) {
                fileout.write(bytes, 0, i);
                i = in.read(bytes);
            }
            fileout.close();
            in.close();
            System.out.println("Fichero descifrado con clave secreta.");
        } catch (IOException
                | InvalidKeyException
                | NoSuchAlgorithmException
                | NoSuchPaddingException e) {
            System.out.println(e.toString());
        }

    }

    /**
     * Empezamos introduciendo el nombre del archivo para leer
     * hacemos un input del fichero con el nombre introducido
     * Obtenemos los datos del fichero para hacer un HASH y compararlo posteriormente
     * Escribimos el nombre del archivo con el que haremos la comparación
     * sacamos los datos del archivo y lo comparamos al final para comprobar que el Hash el es mismo          
     */
    public static void lecturaHashArchivo() {
        Scanner lector = new Scanner(System.in);

        try {
            System.out.println("Escriba el nombre del fichero para leer");
            String lectura = lector.nextLine();
            
            FileInputStream archivoEntrada = new FileInputStream("hash/" + lectura + ".dat");
            ObjectInputStream ois = new ObjectInputStream(archivoEntrada);
            
            Object dadesMissatge = ois.readObject();
            String dades = (String) dadesMissatge;
            
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            digest.reset();
            digest.update(dades.getBytes("utf8"));
            String sha = String.format("%040x", new BigInteger(1, digest.digest()));

            System.out.println("Escriba el nombre del archivo hash para comprobar que no está modificado");
            String lecturaHash = lector.nextLine();
            
            FileInputStream archivoEntradaHash = new FileInputStream("hash/" + lecturaHash + ".dat");
            ObjectInputStream ois2 = new ObjectInputStream(archivoEntradaHash);

            // Obtenim la cadena
            Object dadesMissatgeHash = ois2.readObject();
            String dadesHash = (String) dadesMissatgeHash;

            if (dadesHash.equalsIgnoreCase(sha)) {               
                System.out.println("El mensaje no ha sido alterado, su contenido es: "+dades);
                System.out.println("BUEN TRABAJO!!");
            } else {                
                System.out.println("El mensaje ha sido alterado, la misión corre peligro");
            }

            archivoEntrada.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Primero introducimos un código hash.
     * Nos pide que escribamos el nombre del archivo con el que queremos comparar
     * Sacamos los datos el archivo y comparamos el código escrito por nosotros y el código que tenía el archivo
     */
    public static void lecturaHash() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce el HASH (Formato Hexadecimal)");

        // Llegeixo el missatge hash
        FileInputStream archivoEntradaHash;
        try {
                        
            System.out.println("Per exemple: " + "'" +"57bf81772419e99604a6c9b740f87b99908eb4aa0381ed2f918f284ead8075ca26da32982a0896672b0ce9f22f074de72de00c4522aeb0fd3af999789dba7918" + "'");
            String comprobar = lector.nextLine();
            
            System.out.println("Introduce el nombre del archivo que guarda el mensaje:");
            String archivo = lector.nextLine();
            
            // Llegeixo el missatge
            FileInputStream archivoEntrada = new FileInputStream("hash/" + archivo + ".dat");
            ObjectInputStream ois = new ObjectInputStream(archivoEntrada);

            // Obtenim la cadena
            Object dadesMissatge = ois.readObject();
            String dades = (String) dadesMissatge;
            
            if (dades.equalsIgnoreCase(comprobar)) {
                System.out.println("El missatge no ha estat alterat!!");
            }else{
                System.out.println("El missatge ha sigut alterat!!");
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Este metodo genera 2 archivos y los almacena en la carpeta "hash".
     * El nombre de cada archivo es escogido por el usuario.
     * El primer archivo contiene un mensaje elegido por el usuario.
     * A continuacion se crea el HASH del mensaje introducido por el usuario y lo almacena en el segundo archivo
     * Finalmente nos muestra por pantalla el mensaje introducido con su respectivo HASH
     */
    public static void generacioHash() {
        FileOutputStream fos_archivoMensaje = null;
        FileOutputStream fos_archivoHash = null;
        File directorio = new File("hash");
        directorio.mkdir();

        //Introduccion de datos por teclado
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce el nombre del fichero que guardará el mensaje. Por ejemplo 'mensaje.dat'");
        String nombreArchivoMensaje = teclado.nextLine();
        System.out.println("Introduce el nombre del fichero que guardará el Hash del mensaje. Por ejemplo 'mensajeHash.dat'");
        String nombreArchivoHash = teclado.nextLine();
        System.out.println("Introduce el mensaje. Por ejemplo 'Hola'");
        String mensaje = teclado.nextLine();
        try {
            fos_archivoMensaje = new FileOutputStream("hash/" + nombreArchivoMensaje+".dat");
            fos_archivoHash = new FileOutputStream("hash/" + nombreArchivoHash+".dat");
            ObjectOutputStream oos_mensaje = new ObjectOutputStream(fos_archivoMensaje);
            ObjectOutputStream oos_hash = new ObjectOutputStream(fos_archivoHash);

            //Escritura del mensaje en su archivo
            oos_mensaje.writeObject(mensaje);

            //Creacion SHA-1            
            //Creacion SHA-512            
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            digest.reset();
            digest.update(mensaje.getBytes("utf8"));
            String sha = String.format("%040x", new BigInteger(1, digest.digest()));

            //Imprimo
            System.out.println("El teu missatge és: " + mensaje);
            System.out.println("El HASH (SHA-512): " + sha);

            //Escritura del SHA-1 en su archivo
            System.out.println("Tu mensaje es: " + mensaje);
            System.out.println("El HASH (SHA-512): " + sha);

            //Escritura del SHA-512 en su archivo
            oos_hash.writeObject(sha);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos_archivoMensaje.close();
                fos_archivoHash.close();
            } catch (IOException ex) {
                Logger.getLogger(TVirus.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
