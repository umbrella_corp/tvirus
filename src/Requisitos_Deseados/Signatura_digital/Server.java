/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Signatura_digital;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mgisa
 */
public class Server {
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
    
        Socket socket = null;
        ServerSocket servidorSocket;
        Conexion conex;
        
        try {
            servidorSocket = new ServerSocket(12346);
            System.out.println("Server abierto esperando cliente...");
                           
                socket = servidorSocket.accept();
                conex = new Conexion(socket);
                conex.start();
            
            
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
