/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Signatura_digital;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mgisa
 */
public class Client {

    /**
     *Inicializamos el socket y los objetos de lectura y escritura
     * 
     * Recibimos la clave pública, el tipado de la signatura, el mensaje y la firma
     * 
     * Hacemos la verificación y enviamos el resultado al servidor
     * @param args
     */
    public static void main(String[] args) {
        String ip = "127.0.0.1";
        final int port = 12346;
        
        Socket socket;
        ObjectOutputStream dar;
        ObjectInputStream recibir;
        System.out.println("Esperando mensaje de la organización...");
        try {
            socket = new Socket(ip, port);
            dar = new ObjectOutputStream(socket.getOutputStream());
            recibir = new ObjectInputStream(socket.getInputStream());
            
            
            PublicKey publicKeyServer = (PublicKey) recibir.readObject();
            System.out.println("Clave pública del servidor recibida");
            
            
            String tipado = (String) recibir.readObject();
                                                                    
            String mensaje = (String) recibir.readObject();
            System.out.println("Mensaje del servidor recibido");

                                    
            byte[] firma = (byte[]) recibir.readObject();
            System.out.println("Firma del servidor recibida");
            System.out.println("Verificación en curso...");
            
            
            Signature verificadsa = Signature.getInstance(tipado);
            verificadsa.initVerify(publicKeyServer);
            verificadsa.update(mensaje.getBytes());
            boolean check = verificadsa.verify(firma);
            if (check) {
                System.out.println("SIGNATURA VERIFICADA");
                System.out.println("Contenido del mensaje: "+mensaje.toString());
                dar.writeObject(check);
                
            } else {
                System.out.println("SIGNATURA NO VERIFICADA, LA MISIÓN CORRE PELIGRO!");
                dar.writeObject(check);
            }
            
            dar.close();
            recibir.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
