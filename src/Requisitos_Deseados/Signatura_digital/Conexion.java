/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Signatura_digital;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mgisa
 */
public class Conexion extends Thread {

    private Socket socket;
    private ObjectOutputStream dar;
    private ObjectInputStream recibir;

    /**
     *
     * @param socket
     */
    public Conexion(Socket socket) {
        this.socket = socket;
    }

    /**
     * Creamos un generador de claves tipo "DSA" y lo incicializamos
     * 
     * Creamos una clave pública y otra privada.
     * La pública la pasamos al cliente y la privada la usamos para la signatura del mensaje
     * 
     * Una vez tenemos la signatura creada, enviamos al cliente el tipo de la signatura y el mensaje.
     * 
     * Creamos una firma introduciendo la signatura y el mensaje para enviar y la enviamos al cliente
     * Ahora el cliente tendrá los datos suficientes para la verificación
     * 
     * Al final recibiremos un mensaje del cliente diciendo si ha podido verificar correctamente el mensaje y no ha sido manipulado
     */
    @Override
    public void run() {
        Scanner lector = new Scanner(System.in);
        try {
            dar = new ObjectOutputStream(socket.getOutputStream());
            recibir = new ObjectInputStream(socket.getInputStream());
            
            KeyPairGenerator generarKey = KeyPairGenerator.getInstance("DSA");
                                        
            SecureRandom num = SecureRandom.getInstance("SHA1PRNG");
            generarKey.initialize(1024);
            
            KeyPair parKeys = generarKey.generateKeyPair();
            PrivateKey clavePrivada = parKeys.getPrivate();
            PublicKey clavePublica = parKeys.getPublic();
            
            Signature signa = Signature.getInstance("SHA1withDSA");
            String tipoSignatura = "SHA1withDSA";
            
            signa.initSign(clavePrivada);
            
            System.out.println("Escriba el mensaje secreto:");
            String mensaje = lector.nextLine();            
            dar.writeObject(clavePublica);            
            dar.writeObject(tipoSignatura);
            dar.writeObject(mensaje);
            System.out.println("Enviando mensaje...");
            signa.update(mensaje.getBytes());
            byte[] firma = signa.sign();
            dar.writeObject(firma);

            boolean destruccion = (boolean) recibir.readObject();
            if (destruccion) {
                System.out.println("Mensaje enviado correctamente!");
            } else {
                System.out.println("Mensaje interceptado! Autodestrucción en 5 segundos...");
            }
            dar.close();

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
