/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Criptografia_Asimetrica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 * @author David
 */
public class Cliente {

    /**
     * La función del Cliente es conectarse con el Servidor, este recibirá del
     * Servidor la Clave pública. Con el KeyGenerator generará una clave y esta
     * clave será introducida en el interior de un fichero TXT que estará
     * envuelto (WRAP) por la clave pública recibida por parte del Servidor. Por
     * último, el cliente le mandará al Servidor la clave envuelta.
     */
    public static void main(String[] args) {
        ObjectInputStream recibeDelServidor = null;
        ObjectOutputStream enviaAlServidor = null;
        Socket socket = null;
        String direccionIPCliente = "127.0.0.1";
        try {
            socket = new Socket(direccionIPCliente, 12345);
            recibeDelServidor = new ObjectInputStream(socket.getInputStream());
            enviaAlServidor = new ObjectOutputStream(socket.getOutputStream());

            PublicKey clavePublicaServidor = (PublicKey) recibeDelServidor.readObject();

            KeyGenerator generadorDeClaves = KeyGenerator.getInstance("AES");
            generadorDeClaves.init(128);
            SecretKey claveSecreta = generadorDeClaves.generateKey();

            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.WRAP_MODE, clavePublicaServidor);
            byte claveEnvuelta[] = c.wrap(claveSecreta);
            FileOutputStream ficheroConClaveSecretaEnvuelta = new FileOutputStream("EnigmaAesWrapRsa.txt");
            ObjectOutputStream escritorOOS = new ObjectOutputStream(ficheroConClaveSecretaEnvuelta);
            escritorOOS.writeObject(claveEnvuelta);

            enviaAlServidor.writeObject(claveEnvuelta);
            //CIFRAR UN ARCHIVO
            CifrarFichero(claveSecreta);
        } catch (IOException | ClassNotFoundException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Este método comparte mucho de lo cometado en el TVirus.java. Serviría
     * para cifrar la imagen TVirus.jpg (IMPORTANTE, DICHA IMAGEN DEBE ESTAR EN
     * LA CARPETA RESOURCES PARA QUE EL CIFRADO SURGA EFECTO). Con la clave
     * generada anteriormente cifraremos la imagen.
     *
     * @param claveSecreta Sirve para cifrar un archivo
     */
    public static void CifrarFichero(SecretKey claveSecreta) {
        Cipher cipher;
        try {
            File crearDirectorioCrypt = new File("crypt/");
            crearDirectorioCrypt.mkdirs();
            FileOutputStream creadorFicheroCifrado = new FileOutputStream("crypt/TVirus.jpg.aes");

            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, claveSecreta);

            File crearDirectorioResources = new File("resources/");
            crearDirectorioResources.mkdirs();
            FileInputStream ficheroQueSeQuiereCifrar = new FileInputStream("resources/TVirus.jpg");

            CipherOutputStream out = new CipherOutputStream(creadorFicheroCifrado, cipher);
            int tamanoBloque = cipher.getBlockSize();
            byte[] bytes = new byte[tamanoBloque];

            int i = ficheroQueSeQuiereCifrar.read(bytes);
            while (i != -1) {
                out.write(bytes, 0, i);
                i = ficheroQueSeQuiereCifrar.read(bytes);
            }
            out.flush();
            out.close();
            ficheroQueSeQuiereCifrar.close();
            System.out.println("Fichero cifrado con clave secreta.");
        } catch (IOException
                | InvalidKeyException
                | NoSuchAlgorithmException
                | NoSuchPaddingException e) {
            System.out.println(e.toString());
        }

    }

}
