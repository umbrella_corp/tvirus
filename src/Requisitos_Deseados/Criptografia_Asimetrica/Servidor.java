/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Criptografia_Asimetrica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 * @author David
 */
public class Servidor {

    /**
     * El método main se encargará de iniciar la conexión
     */
    public static void main(String[] args) {
        Socket socket = null;
        ServerSocket servidorSocket;
        try {
            servidorSocket = new ServerSocket(12345);
            while (true) {
                socket = servidorSocket.accept();
                (new Servidor.Conexion(socket)).start(); //INICIO EL HILO DE LA CONEXIÓN
            }
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * La clase Conexión lanzará un hilo nuevo por cada conexión con el Cliente
     * y realizar las tareas establecidas en el run.
     */
    public static class Conexion extends Thread {

        private Socket socket;
        private ObjectInputStream recibeDelCliente;
        private ObjectOutputStream enviaAlCliente;

        public Conexion(Socket socket) {
            this.socket = socket;
        }

        /**
         *
         * El método run generará las claves públicas y privadas. La pública se
         * la enviará al Cliente y la privada se la quedará para desenrollar
         * (UNWRAP) el TXT generado y enrollado por el Cliente. La clave en que
         * había dentro del TXT servirá para descrifar la imagen cifrada por el
         * Cliente.
         */
        @Override
        public void run() {
            try {
                enviaAlCliente = new ObjectOutputStream(socket.getOutputStream());
                System.out.println(socket.getInetAddress());
                recibeDelCliente = new ObjectInputStream(socket.getInputStream());

                KeyPairGenerator generadorDeClaves;
                generadorDeClaves = KeyPairGenerator.getInstance("RSA");
                generadorDeClaves.initialize(1024);
                KeyPair par = generadorDeClaves.generateKeyPair();
                PrivateKey clavePrivada = par.getPrivate();
                PublicKey clavePublica = par.getPublic();

                enviaAlCliente.writeObject(clavePublica);

                System.out.println("CLAVE ENVIADA");

                Cipher cipherServidor = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipherServidor.init(Cipher.UNWRAP_MODE, clavePrivada);

                byte claveEnvuelta[] = (byte[]) recibeDelCliente.readObject();
                Key claveDesenvuelta = cipherServidor.unwrap(claveEnvuelta, "AES", Cipher.SECRET_KEY);
                ObjectInputStream oin = new ObjectInputStream(new FileInputStream("EnigmaAesWrapRsa.txt"));
                Key claveSecretaParaDesencriptar = claveDesenvuelta;
                oin.close();

                DescifrarFichero((SecretKey) claveSecretaParaDesencriptar);

            } catch (IOException | NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | ClassNotFoundException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (enviaAlCliente != null) {
                        enviaAlCliente.close();
                    }
                    if (recibeDelCliente != null) {
                        enviaAlCliente.close();
                    }
                    if (socket != null) {
                        enviaAlCliente.close();
                    }
                } catch (Exception e) {
                }
            }

        }

        /**
         * Este método comparte mucho de lo cometado en el TVirus.java. Serviría
         * para descifrar la imagen TVirus.jpg (IMPORTANTE, DICHA IMAGEN CIFRADA
         * DEBE ESTAR EN LA CARPETA CRYPT PARA QUE EL DESCIFRADO SURGA EFECTO).
         * Con la clave obtenida del interior del fichero TXT descifraremos la
         * imagen o del Cliente.
         *
         * @param claveSecreta Sirve para descifrar un archivo
         */
        public static void DescifrarFichero(SecretKey claveSecreta) {

            Cipher cipher;
            try {
                cipher = Cipher.getInstance("AES");
                cipher.init(Cipher.DECRYPT_MODE, claveSecreta);
                File crearDirectorioCrypt = new File("crypt/");
                crearDirectorioCrypt.mkdirs();
                CipherInputStream leerCipher = new CipherInputStream(
                        new FileInputStream("crypt/TVirus.jpg.aes"), cipher);
                int tamanoBloque = cipher.getBlockSize();
                byte[] bytes = new byte[tamanoBloque];
                tamanoBloque = cipher.getBlockSize();
                bytes = new byte[tamanoBloque];

                FileOutputStream ficheroDesencriptado = new FileOutputStream("crypt/TVirus.jpg");

                int i = leerCipher.read(bytes);
                while (i != -1) {
                    ficheroDesencriptado.write(bytes, 0, i);
                    i = leerCipher.read(bytes);
                }
                ficheroDesencriptado.close();
                leerCipher.close();
                System.out.println("Fichero descifrado con clave secreta.");
            } catch (IOException
                    | InvalidKeyException
                    | NoSuchAlgorithmException
                    | NoSuchPaddingException e) {
                System.out.println(e.toString());
            }

        }

    }

}
