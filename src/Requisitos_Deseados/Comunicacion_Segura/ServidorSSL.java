/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Comunicacion_Segura;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 * Clase que hace de servidorSSL para enviar un mensaje a un cliente conectado y posteriormente
 * comprobar si el mensaje ha sido modificado por este.
 * @author Carlos
 */
public class ServidorSSL extends Thread {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        System.setProperty("javax.net.ssl.keyStore", "certificadosSSL\\ServerSSLKeyStore");
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");

        ServerSocketFactory ssf = SSLServerSocketFactory.getDefault();
        ServerSocket ss = ssf.createServerSocket(6000);
        System.out.println("Esperando conexion");
        while (true) {
            new ServidorSSL((SSLSocket) ss.accept()).start();
        }

    }

    private SSLSocket sock;

    /**
     * Constructor de la clase ServidorSSL, que nos permite pasarle un parámetro de tipo
     * SSLSocket
     * @param s
     */
    public ServidorSSL(SSLSocket s) {
        sock = s;
    }

    /**
     * El método run nos permetirá ejecutar el hilo a nuestro antojo mediante el metodo start.
     * Este método nos pide un mensaje que será enviado posteriormente al cliente para que este haga su debida encriptación.
     * Despues de introducir el mensaje, tenemos un pequeño menú que nos permite elejir 
     * el tipo de encriptación que deseamos que tenga el mensaje.
     * A continuación envia el mensaje y el tipo de encriptación deseada a el cliente.
     * Por último, espera hasta recibir del cliente el mensaje. Este vuelve a encriptarse y compara
     * si el mensaje ha sido modificado.
     */
    public void run() {
        try {
            Scanner teclado = new Scanner(System.in);
            System.out.println("Cliente conectado...");

            //Flujos de entrada y salida
            DataInputStream dis = new DataInputStream(sock.getInputStream());
            DataOutputStream dos = new DataOutputStream(sock.getOutputStream());

            //Pido los datos para el cliente y los trato para enviarlos
            System.out.println("Escribe el mensaje que deseas recibir calculado:");
            String mensajeOriginal = teclado.nextLine();
            System.out.println("Selecciona uno de los algoritmos que deseas utilizar:");
            int opcion = 0;
            boolean salir = false;
            while (!salir) {
                System.out.println("1-SHA-1");
                System.out.println("2-SHA-512");
                System.out.println("3-MD5");
                opcion = teclado.nextInt();
                if (opcion == 1) {
                    salir = true;
                } else {
                    if (opcion == 2) {
                        salir = true;
                    } else {
                        if (opcion == 3) {
                            salir = true;
                        } else {
                            System.out.println("Opcion incorrecta, introduce una opcion válida:");
                        }

                    }
                }
            }

            String opcionResuelta = resolverOpcion(opcion);
            String result = codificar(mensajeOriginal, opcionResuelta);
            //System.out.println(result);

            //Envio los datos al cliente
            dos.writeUTF(mensajeOriginal);
            dos.writeUTF(opcionResuelta);

            //Recibo la respuesta
            System.out.println("Esperando respuesta ...");
            String respuestaCliente = dis.readUTF();
            System.out.println("Respuesta recibida. El mensaje codificado es: " + respuestaCliente);

            //Comparacion para saber si ha estado modificado
            if (result.equals(respuestaCliente)) {
                System.out.println("El mensaje no ha sido modificado. Todo esta en orden");
            } else {
                System.out.println("CUIDADO! El mensaje ha sido modificado!");
            }

            dos.close();
            dis.close();
            sock.close();

        } catch (IOException ex) {
        }

    }

    /**
     * Método que sirve para recibir una string correspondiente a un tipo de codificacion.
     * De esta manera, cuando en el run se muestra el menu de seleccion de codificación, solo hay que llamar a este metodo
     * pasandole la opcion marcada por el usuario, y este se encarga mediante un switch de 
     * seleccionar la codificación correcta.
     * @param opcion
     * @return
     */
    public static String resolverOpcion(int opcion) {
        String codificacion = "";
        switch (opcion) {
            case 1:
                codificacion = "SHA-1";
                break;
            case 2:
                codificacion = "SHA-512";
                break;
            case 3:
                codificacion = "MD5";
                break;
        }
        return codificacion;
    }

    /**
     * Método que se encarga de codificar el mensaje deseado. A este se le introducen el mensaje
     * y el tipo de codificación deseada, para posteriormente hacer la codificación con el
     * digest y retornar el resultado de el mensaje ya codificado.
     * @param mensajeOriginal
     * @param opcionResuelta
     * @return
     */
    public static String codificar(String mensajeOriginal, String opcionResuelta) {
        String result = "";

        try {
            MessageDigest digest;
            digest = MessageDigest.getInstance(opcionResuelta);
            digest.reset();
            digest.update(mensajeOriginal.getBytes("utf8"));
            result = String.format("%040x", new BigInteger(1, digest.digest()));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ServidorSSL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ServidorSSL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
