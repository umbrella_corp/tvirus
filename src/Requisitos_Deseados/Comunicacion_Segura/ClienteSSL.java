/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Requisitos_Deseados.Comunicacion_Segura;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 * Clase que se conecta a un servidorSSL y que recibe un mensaje y un tipo de codificación deseada.
 * El cliente permite elegir si modificarlo o no y a continuacion lo encripta en la codificación deseada y
 * lo devuelve al servidor.
 * @author Carlos
 */
public class ClienteSSL {

    /**
     * El método main se encarga de conectarse al servidor mediante el SSLSocket.
     * Despues recibe los datos del servidor (mensaje y tipo de codificación)
     * Seguidamente pregunta al usuario si desea modificar o no el mensaje mediante un pequeño menú.
     * Despues de modificarlo (o no) se vuelve a mandar el mensaje con su codificacion deseada
     * a el servidor, para que este compurbe si ha sido alterado.
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Scanner teclado = new Scanner(System.in);
        String Host = "localhost";
        int port = 6000;

        System.setProperty("javax.net.ssl.trustStore", "certificadosSSL\\UserSSLKeyStore");
        System.setProperty("javax.net.ssl.trustStorePassword", "654321");

        System.out.println("Cliente iniciado, esperando a que el servidor nos envie la informacion deseada ...");

        // Obtener el SocketSSL utilizando el patrón factory
        SSLSocketFactory sfact = (SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket client = (SSLSocket) sfact.createSocket(Host, port);

        // Flujos de entrada y salida
        DataInputStream dis = new DataInputStream(client.getInputStream());
        DataOutputStream dos = new DataOutputStream(client.getOutputStream());

        // Recibo los datos del servidor
        String mensaje = dis.readUTF();
        String codificacion = dis.readUTF();

        System.out.println("El mensaje recibido es '" + mensaje + "', y su encriptacion deseada es '"+codificacion+"'. Quieres modificar el mensaje antes de enviarlo codificado?");
        System.out.println("1-Si");
        System.out.println("2-No");
        boolean salir = false;
        while (!salir) {
            int opcion = teclado.nextInt();
            if (opcion == 1) {
                System.out.println("Introduce el nuevo mensaje:");
                teclado.nextLine(); //nextline() para consumir el \n no lanzado por el nextInt() anterior
                mensaje = teclado.nextLine();
                System.out.println("Mensaje modificado");
                salir = true;
            } else {
                if (opcion == 2) {
                    System.out.println("Mensaje no modificado");
                    salir = true;
                } else {
                    System.out.println("Opción no válida. Por favor, introduce una opcion válida:");
                }
            }
        }

        // Coodifico el mensaje
        String respuesta = codificar(mensaje, codificacion);

        // Envio el mensaje codificado
        dos.writeUTF(respuesta);
        System.out.println("Mensaje codificado enviado");

        // Cierro recursos
        dis.close();
        dos.close();
        client.close();
    }

    /**
     * Método que sirve para codificar el mensaje recibido por el servidor, en la codificacion deseada
     * por este. Introduce el mensaje a codificar y el tipo de codificación, y devuelve el mensaje
     * ya codificado.
     * @param mensajeOriginal
     * @param opcionResuelta
     * @return
     */
    public static String codificar(String mensajeOriginal, String opcionResuelta) {
        String result = "";

        try {
            MessageDigest digest;
            digest = MessageDigest.getInstance(opcionResuelta);
            digest.reset();
            digest.update(mensajeOriginal.getBytes("utf8"));
            result = String.format("%040x", new BigInteger(1, digest.digest()));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ServidorSSL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ServidorSSL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

}
